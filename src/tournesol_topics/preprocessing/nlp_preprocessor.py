# -*- coding: utf-8 -*-
from typing import Iterable, Optional, Dict, Tuple
from pathlib import Path
from tqdm.auto import tqdm

from langdetect import detect, LangDetectException

from tournesol_topics import utils
from tournesol_topics.preprocessing.strategies import StrategyFactory
from tournesol_topics.preprocessing.base import BaseStrategy
from tournesol_topics import types as tp


class Preprocessor:
    def __init__(self, strategy_factory: "StrategyFactory"):
        self.strategy_factory = strategy_factory
        self.language_strategies: dict[str, BaseStrategy] = {}
        self.language_mapping = {
            "en": "english",
            "fr": "french",
        }
        self.strategies_cache: dict[str, BaseStrategy] = {}
        self.default_strategy_mapping = {"en": tp.StrategyType.SPACY, "fr": tp.StrategyType.SPACY}

    def add_strategy(self, language: str, strategy: BaseStrategy):
        self.language_strategies[language] = strategy

    def _detect_language(self, text):
        try:
            return detect(text)
        except LangDetectException:
            return ""

    def set_language(self, language: str):
        self.current_strategy = self.get_strategy(language)

    def configure_strategies(
        self, strategy_mapping: Dict[str, Tuple[tp.StrategyType, Optional[list]]] = None, **kwargs
    ):
        """
        Configure which strategy to use for each language.

        Args:
            strategy_mapping: Dict mapping languages to (strategy_type, additional_stopwords)
            **kwargs: Additional configuration parameters

        Example:
            {
                "english": (StrategyType.SPACY, ["custom_stopword"]),
                "french": (StrategyType.NLTK, None)
            }
        """
        if strategy_mapping:
            for language, (strategy_type, stopwords) in strategy_mapping.items():
                config = tp.StrategyConfig(language=language, additional_stopwords=stopwords, **kwargs)
                self.strategies_cache[language] = self.strategy_factory.create_strategy(
                    strategy_type=strategy_type, config=config, language=language
                )

    def get_strategy(self, language: str) -> BaseStrategy:
        """Get or create strategy for given language"""
        if language not in self.strategies_cache:
            strategy_type = self.default_strategy_mapping.get(language, tp.StrategyType.NLTK)
            config = tp.StrategyConfig(language=language)
            self.strategies_cache[language] = self.strategy_factory.create_strategy(
                strategy_type=strategy_type, config=config, language=language
            )
        return self.strategies_cache[language]

    def prepare_data(self, data: list[dict], var_name: str):
        texts = []
        indices = []
        languages = []
        for i, metadata in tqdm(enumerate(data), desc="Preprocessing language detection", total=len(data)):
            transcript = metadata[var_name].lower()
            language_code = metadata.get("language", self._detect_language(transcript))
            language = self.language_mapping.get(language_code, None)
            texts.append(transcript)
            languages.append(language)
            indices.append(i)
        return {"texts": texts, "languages": languages, "indices": indices}

    def preprocess_and_tag(self, text, **kwargs) -> list[str]:
        tokenized_text = self.current_strategy.preprocess_and_tag(text)
        tokenized_text = [utils.process_string(token, **kwargs) for token in tokenized_text]
        tokenized_text = [token.strip() for token in tokenized_text if len(token) > 2]
        return tokenized_text

    def post_process_data(self, data: list, var_name, indices, processed_texts):
        new_data = []
        for i, text in zip(indices, processed_texts):
            data[i][var_name] = self.preprocess_and_tag(text)
            new_data.append(data[i])
        return new_data

    def save_data(self, data, filename):
        utils.save_jsonl(data, filename)

    def process_single_text(
        self,
        text: str,
        strategy_mapping: Dict[str, Tuple[tp.StrategyType, Optional[list]]] = None,
        language: str = "french",
    ) -> list[str]:
        """
        This method preprocesses, tokenizes and tags a single text based on the given language.

        Args:
            text (str): The text to be processed.
            language (str): The language of the text.

        Returns:
            str: The preprocessed, tokenized and tagged text.
        """
        if strategy_mapping:
            self.configure_strategies(strategy_mapping)
        self.set_language(language)
        tokenized_text = self.current_strategy.tokenize_docs([text], n_process=1)[0]
        tagged_text = self.preprocess_and_tag(tokenized_text)

        return tagged_text

    @utils.timer
    def run(
        self,
        data,
        var_name,
        save=True,
        strategy_mapping: Dict[str, Tuple[tp.StrategyType, Optional[list]]] = None,
        languages: Iterable[str] = ("french",),
        filename: Path | str = Path.cwd() / "preprocessed_data.jsonl",
    ):
        if strategy_mapping:
            self.configure_strategies(strategy_mapping)

        all_processed_data = []
        for language in languages:
            self.set_language(language)
            prepared_data = self.prepare_data(data=data, var_name=var_name)
            texts = prepared_data["texts"]
            indices = prepared_data["indices"]
            languages = prepared_data["languages"]
            lang_texts = [text for text, lang in zip(texts, languages) if lang == language]
            lang_indices = [idx for idx, lang in zip(indices, languages) if lang == language]

            tokenized_docs = self.current_strategy.tokenize_docs(lang_texts)

            processed_data = self.post_process_data(
                data=data,
                var_name="processed_transcripts",
                indices=lang_indices,
                processed_texts=tokenized_docs,
            )
            all_processed_data.extend(processed_data)
            if save:
                print("Saving preprocessed data in ", filename)
                self.save_data(data=processed_data, filename=filename)
        return all_processed_data
