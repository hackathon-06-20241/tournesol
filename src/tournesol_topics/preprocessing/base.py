from typing import Union
from abc import ABC, abstractmethod
import spacy
import spacy.tokens


class BaseStrategy(ABC):

    @abstractmethod
    def preprocess_and_tag(self, docs: Union[spacy.tokens.Doc, list[tuple[str, str]]]):
        raise NotImplementedError

    @abstractmethod
    def get_stopwords(self):
        raise NotImplementedError

    @abstractmethod
    def tokenize_docs(self, docs, **kwargs) -> list:
        raise NotImplementedError


class AbstractPreprocessor(ABC):
    @abstractmethod
    def preprocess_and_tag(self, doc):
        pass

    @abstractmethod
    def run(self, data, var_name, save=True, filename=None, **kwargs):
        pass
