import re
from unidecode import unidecode
from nltk.stem import WordNetLemmatizer
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop
from nltk.stem.porter import PorterStemmer
import pandas as pd

import thefuzz
from thefuzz import process


def remove_fr_punct(list_text):
    list_propre = []
    for word in list_text:
        word = unidecode(word)
        word = word.replace("'", " ")
        # remove all letters alone
        word = " ".join([w for w in word.split() if len(w) > 2])
        list_propre.append(word)
    return list_propre


def text_clean(text):
    lower = text.lower()
    no_tab = lower.replace("\n", " ")
    split_text = re.split("W+", no_tab)

    no_punc = remove_fr_punct(split_text)

    # remove empty
    full_list = list(filter(None, no_punc))

    return full_list


def stemming_word(text):
    # defining the object for stemming
    porter_stemmer = PorterStemmer()
    stem_text = [porter_stemmer.stem(word) for word in text]

    return stem_text


def lemmatizer(text):
    wordnet_lemmatizer = WordNetLemmatizer()
    lemm_text = [wordnet_lemmatizer.lemmatize(word) for word in text]
    return lemm_text


def remove_stop(text):
    french_stop = list(fr_stop)
    clean_list = []
    for elem in text:
        if elem not in french_stop:
            clean_list.append(elem)

    return clean_list


def fuzzy_match(string_to_match, targets, limit=3):
    """Computes string distance from a string to targets
        using levenstein distance implemented in thefuzz package
        (https://github.com/seatgeek/thefuzz/tree/master)

    Args:
        string_to_match (str): string to be tested
        targets (list of str): possible target to compare to
        limit (int, optional): minimum of best comparaisons to return

    Returns:
        list of tuples: tuples indicating closer strings
            ("target", score)
    """
    return process.extract(query=string_to_match, choices=targets, limit=limit)


def fuzzy_match_sequences(test, target, scorer=thefuzz.fuzz.ratio):
    """Computes best string distance comparing exhaustively
        all strings from source to alls string to targets and getting the best matches
        using levenstein distance implemented in thefuzz package
        (https://github.com/seatgeek/thefuzz/tree/master)


    Args:
        test (list of str): list of variable to compare to target
        target (list of str): list of potential target
        scorer (func): type of scorer (default : levenstein distance ration)
    Returns:
        dataframe: dataframe with tested variable, target variable and score.
    """
    results = {"matches": [], "score": []}

    for item in target:
        ratios = process.extractOne(item, target, scorer)
        results["matches"].append(ratios[0])
        results["score"].append(ratios[1])

    df = pd.DataFrame({"tested": test, "matches": results["matches"], "score": results["score"]})
    return df
