from typing import Dict, Type, Optional

from tournesol_topics.preprocessing.base import BaseStrategy


class StrategyRegistry:
    _strategies: Dict[str, Dict] = {}

    @classmethod
    def register(cls, strategy_name: str, description: str = "", languages: Optional[set[str]] = None):
        """Enhanced decorator with metadata"""

        def decorator(strategy_class: Type["BaseStrategy"]):
            cls._strategies[strategy_name.lower()] = {
                "class": strategy_class,
                "description": description,
                "languages": languages or [],
            }
            return strategy_class

        return decorator

    @classmethod
    def get_strategy_info(cls) -> dict:
        """Get detailed information about all strategies"""
        return {
            name: {"description": info["description"], "languages": info["languages"]}
            for name, info in cls._strategies.items()
        }

    @classmethod
    def get_strategy(cls, strategy_name: str) -> Type["BaseStrategy"]:
        """Get a strategy class by name"""
        strategy_info = cls._strategies.get(strategy_name.lower())
        if not strategy_info:
            raise ValueError(
                f"Unknown strategy: {strategy_name}. Available strategies: {list(cls._strategies.keys())}"
            )
        return strategy_info["class"]

    @classmethod
    def get_languages(cls, strategy_name: str) -> list[str]:
        """Get the languages supported by a strategy"""
        strategy_info = cls._strategies.get(strategy_name.lower())
        if not strategy_info:
            raise ValueError(
                f"Unknown strategy: {strategy_name}. Available strategies: {list(cls._strategies.keys())}"
            )
        return strategy_info["languages"]


if __name__ == "__main__":
    print(StrategyRegistry.get_strategy_info())
