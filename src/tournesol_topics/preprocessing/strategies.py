from typing import Optional

import spacy.cli
import spacy.cli.download
from tqdm.auto import tqdm
import multiprocessing
import spacy
import spacy.tokens
from spacy.language import Language

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag import pos_tag

from tournesol_topics import types as tp
from tournesol_topics import constants
from tournesol_topics.preprocessing.base import BaseStrategy
from tournesol_topics.preprocessing.preprocessor_register import StrategyRegistry


@StrategyRegistry.register(
    "spacy",
    description="SpaCy-based text processing strategy",
    languages=constants.LANGUAGES,
)
class SpaCyStrategy(BaseStrategy):

    DEFAULT_FILTER_POS = constants.SPACY_DEFAULT_POS_TAGS
    LANGUAGE_MODELS = constants.SPACY_LANGUAGE_MODELS

    def __init__(self, language, additional_stopwords: Optional[list] = None):
        self.language = language
        self.pos_tags = self.DEFAULT_FILTER_POS
        self.disable_components = ["ner", "parser"]
        self.additional_stopwords = additional_stopwords
        self.nlp = self._load_spacy_model()

    def preprocess_and_tag(self, docs: spacy.tokens.Doc) -> list[str]:
        filtered_tokens = [
            token.lemma_
            for token in docs
            if not (token.is_stop or token.is_punct or token.pos_ in self.pos_tags or token.is_digit)
        ]
        filtered_tokens = [token for token in filtered_tokens if token not in self.get_stopwords()]
        return filtered_tokens

    def tokenize_docs(self, docs, **kwargs) -> list[spacy.tokens.Doc]:
        batch_size = kwargs.get("batch_size", 30)
        n_process = kwargs.get("n_process", 1)
        processed_docs = list(
            tqdm(
                self.nlp.pipe(docs, batch_size=batch_size, n_process=n_process),
                total=len(docs),
            )
        )
        return processed_docs

    def get_stopwords(self):
        default_stopwords = set(stopwords.words(self.language))
        if self.additional_stopwords is not None:
            default_stopwords.update(self.additional_stopwords)
        return default_stopwords

    def _convert_to_iso_code(self, language: str) -> str:
        """Convert full language names to ISO codes."""
        language_map = constants.LANGUAGES_MAP
        return language_map.get(language.lower(), language.lower())

    def _load_spacy_model(self) -> Language:
        """Load SpaCy model with optimized settings."""
        model = self._convert_to_iso_code(self.language)
        self.model_name = self.LANGUAGE_MODELS.get(model, model)
        try:
            return spacy.load(self.model_name, disable=self.disable_components)
        except OSError:
            spacy.cli.download(self.model_name)
            return spacy.load(self.model_name, disable=self.disable_components)


@StrategyRegistry.register(
    "nltk",
    description="NLTK-based text processing strategy",
    languages=constants.LANGUAGES,
)
class NLTKStrategy(BaseStrategy):

    DEFAULT_FILTER_POS = constants.NLTK_DEFAULT_POS_TAGS

    def __init__(self, language, additional_stopwords: Optional[list] = None):
        self.language = language
        self.pos_tags = self.DEFAULT_FILTER_POS
        self.additional_stopwords = additional_stopwords

    def preprocess_and_tag(self, docs: list[tuple[str, str]]) -> list[str]:  # type: ignore
        lemmatizer = WordNetLemmatizer()
        filtered_tokens = [
            word
            for token_list in docs
            for word, pos in token_list
            if pos not in self.pos_tags and word.isalpha()
        ]
        filtered_tokens = [lemmatizer.lemmatize(word) for word in filtered_tokens]
        return filtered_tokens

    def tokenize_and_pos_tag(self, text: str) -> list[tuple[str, str]]:
        tokens = word_tokenize(text, language=self.language)
        if self.language == "english":
            tokens = [w for w in tokens if w not in self.get_stopwords()]
            pos_tags = pos_tag(tokens, lang="eng", tagset="universal")
        else:
            # TODO use Stanford POS tagger: https://majumdarsrijoni.medium.com/using-stanford-taggers-nltk-in-3-steps-960e2ad557a4
            pos_tags = [(word, "") for word in tokens if word not in self.get_stopwords()]
        return pos_tags

    def process_chunk(self, chunk) -> list[list[tuple[str, str]]]:
        return [self.tokenize_and_pos_tag(chunk)]

    def tokenize_docs(self, docs, **kwargs) -> list[list[tuple[str, str]]]:
        n_process = kwargs.get("n_process", 2)
        pool = multiprocessing.Pool(processes=n_process)
        with tqdm(total=len(docs), unit="docs", desc="Tokenizing docs") as pbar:
            results = [
                pool.apply_async(self.process_chunk, (doc,), callback=lambda _: pbar.update(1))
                for doc in docs
            ]
            pool.close()
            pool.join()
            results = [result.get() for result in results]
        return results

    def get_stopwords(self):
        default_stopwords = set(stopwords.words(self.language))
        if self.additional_stopwords is not None:
            default_stopwords.update(self.additional_stopwords)
        return default_stopwords


class StrategyFactory:
    """Factory class to create text processing strategies"""

    @classmethod
    def create_strategy(
        cls,
        strategy_type: str,
        config: tp.StrategyConfig,
        language: str,
        additional_stopwords: Optional[list] = None,
    ):
        """
        Create and return a text processing strategy based on the specified type.

        Args:
            strategy_type: The type of strategy to create
            language: The language to process
            additional_stopwords: Optional list of additional stopwords

        Returns:
            An instance of the specified strategy
        """
        cls.validate_language(strategy_type, language)
        strategy_class = StrategyRegistry.get_strategy(strategy_type)
        return strategy_class(language=language, additional_stopwords=additional_stopwords)  # type: ignore

    @staticmethod
    def validate_language(strategy_type: str, language: str):
        """Validate that the language is supported by the strategy"""
        strategy_type = strategy_type.lower()
        strategy_info = StrategyRegistry.get_strategy(strategy_type)
        if not strategy_info:
            return

        supported_languages = StrategyRegistry.get_languages(strategy_type)
        if supported_languages and language not in supported_languages:
            raise ValueError(
                f"Language '{language}' not supported by {strategy_type}. "
                f"Supported languages: {supported_languages}"
            )
