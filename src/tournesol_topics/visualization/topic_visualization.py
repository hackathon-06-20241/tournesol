import matplotlib.pyplot as plt
from bertopic import BERTopic


def compute_top_words_sklearn(model, feature_names, n_top_words):
    """
    Compute the top words for each topic
    """
    topics = []
    for topic_idx, topic in enumerate(model.components_):
        top_features_ind = topic.argsort()[-n_top_words:]
        top_features = [feature_names[i] for i in top_features_ind]
        weights = topic[top_features_ind]
        topics.append((top_features, weights))
    return topics


def plot_top_words(topics, title):
    """
    Plot the top words for each topic
    """
    col = 5
    topic_num = len(topics)
    topic_num_col_ratio = topic_num // col
    row = topic_num_col_ratio if topic_num % col == 0 else topic_num_col_ratio + 1
    fig, axes = plt.subplots(row, 5, figsize=(30, 15), sharex=True)
    axes = axes.flatten()
    for topic_idx, (top_features, weights) in enumerate(topics):
        ax = axes[topic_idx]
        ax.barh(top_features, weights, height=0.7)
        ax.set_title(f"Topic {topic_idx +1}", fontdict={"fontsize": 30})
        ax.tick_params(axis="both", which="major", labelsize=20)
        for i in "top right left".split():
            ax.spines[i].set_visible(False)
        fig.suptitle(title, fontsize=40)

    plt.subplots_adjust(top=0.90, bottom=0.05, wspace=0.90, hspace=0.3)
    plt.savefig(f"{title}.png")


def compute_top_words_gensim(model, n_top_words):
    """
    Compute the top words for each topic
    """
    topics = []
    for topic_idx in range(len(model.get_topics())):
        topic = model.show_topic(topic_idx, topn=n_top_words)
        topic = sorted(topic, key=lambda x: x[1], reverse=False)
        top_features = [word_id for word_id, _ in topic]
        weights = [prob for _, prob in topic]
        topics.append((top_features, weights))
    return topics


def plot_top_words_bertopic(model: BERTopic, n_top_words: int = 10):
    fig = model.visualize_barchart(top_n_topics=40, n_words=n_top_words)
    fig.write_image("bertopic_barchart.png")
