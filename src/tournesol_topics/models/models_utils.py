from bertopic import BERTopic
from gensim.models import LdaMulticore
from skops.io import load, get_untrusted_types


def load_sklearn_model(model_path: str):
    """Loads the LDA model from the specified path."""
    unknown_types = get_untrusted_types(file=model_path)
    return load(model_path, trusted=unknown_types)


def load_bertopic_model(model_path: str):
    return BERTopic.load(path=model_path)


def load_gensim_model(model_path: str):
    """Loads the Gensim LDA model from the specified path."""
    return LdaMulticore.load(fname=model_path)
