from abc import ABC, abstractmethod
from typing import List, Any


class Preprocessor(ABC):
    @abstractmethod
    def preprocess(self, data: List[dict]) -> Any:
        pass


class ModelTrainer(ABC):
    @abstractmethod
    def train(self, data: Any) -> Any:
        pass


class Inference(ABC):
    @abstractmethod
    def infer(self, data: Any) -> Any:
        pass
