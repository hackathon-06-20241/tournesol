import typing as t
from tqdm import tqdm
import collections
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer
from gensim.models import LdaMulticore
from gensim.corpora import Dictionary
import hdbscan
from umap import UMAP
from bertopic import BERTopic
from sentence_transformers import SentenceTransformer
from bertopic.representation import KeyBERTInspired, BaseRepresentation
from skops.io import dump, load, get_untrusted_types
from tournesol_topics.models.base_topic_model import Preprocessor, ModelTrainer


class TopicModelPreprocessor(Preprocessor):
    def __init__(self, stop_words, min_vocab_freq=15, max_vocab_freq=10000):
        self.stop_words = stop_words
        self.min_vocab_freq = min_vocab_freq
        self.max_vocab_freq = max_vocab_freq
        self.vectorizer_model = None

    def process_docs(self, docs: t.List[t.Union[str, t.List[str]]]) -> t.List[str]:
        if all(isinstance(doc, str) for doc in docs):
            return docs  # type: ignore
        return [" ".join(doc) for doc in docs]

    def preprocess(self, docs: t.List[t.List[str]]):
        all_vocab = collections.Counter()  # type: ignore
        tokenizer = CountVectorizer().build_tokenizer()
        docs = self.process_docs(docs)  # type: ignore
        for doc in tqdm(docs):
            all_vocab.update(tokenizer(doc))
        vocab = [
            word
            for word, frequency in all_vocab.items()
            if frequency >= self.min_vocab_freq and frequency < self.max_vocab_freq
        ]
        self.vectorizer_model = CountVectorizer(vocabulary=vocab, stop_words=list(self.stop_words))
        return self.vectorizer_model


class SklearnModelTrainer(ModelTrainer):
    def __init__(
        self,
        vectorizer_model,
        n_components=20,
        max_iter=8,
        random_state=0,
        learning_method="online",
        learning_offset=50,
        n_jobs=-1,
    ):
        self.model = LatentDirichletAllocation(
            n_components=n_components,
            max_iter=max_iter,
            random_state=random_state,
            learning_method=learning_method,
            learning_offset=learning_offset,
            n_jobs=n_jobs,
        )
        self.vectorizer_model = vectorizer_model

    def train(self, data: t.Any):
        tf = self.vectorizer_model.fit_transform(data)
        return self.model.fit_transform(tf)

    def save_model(self, model_path: str, **kwargs):
        dump(self.model, model_path, **kwargs)

    def load_model(self, model_path: str):
        unknown_types = get_untrusted_types(file=model_path)
        self.model = load(model_path, trusted=unknown_types)


class GensimModelTrainer(ModelTrainer):
    def __init__(self, num_topics=10, workers=3, chunksize=100, passes=10, per_word_topics=True):
        self.num_topics = num_topics
        self.workers = workers
        self.chunksize = chunksize
        self.passes = passes
        self.per_word_topics = per_word_topics

    def initialize_models(self, docs: t.List[t.List[str]]):
        dictionary = Dictionary([d for d in docs])
        corpus = [dictionary.doc2bow(doc) for doc in docs]
        return dictionary, corpus

    def train(self, data: t.List[t.List[str]]):
        dictionary, corpus = self.initialize_models(data)
        self.model = LdaMulticore(
            corpus=corpus,
            id2word=dictionary,
            num_topics=self.num_topics,
            workers=self.workers,
            chunksize=self.chunksize,
            passes=self.passes,
            per_word_topics=self.per_word_topics,
        )
        return self.model

    def save_model(self, model_path: str, **kwargs):
        self.model.save(fname=model_path, **kwargs)

    def load_model(self, model_path: str):
        self.model = LdaMulticore.load(fname=model_path)


class BERTopicModelTrainer(ModelTrainer):
    def __init__(
        self,
        embedding_model: t.Any = SentenceTransformer(
            "tomaarsen/static-retrieval-mrl-en-v1", truncate_dim=256
        ),
        umap_n_components=10,
        umap_n_neighbors=30,
        umap_random_state=42,
        umap_metric="cosine",
        hdbscan_min_samples=20,
        hdbscan_min_cluster_size=20,
        representation_model: BaseRepresentation = KeyBERTInspired(),
    ):
        self.embedding_model = embedding_model
        self.umap_n_components = umap_n_components
        self.umap_n_neighbors = umap_n_neighbors
        self.umap_random_state = umap_random_state
        self.umap_metric = umap_metric
        self.hdbscan_min_samples = hdbscan_min_samples
        self.hdbscan_min_cluster_size = hdbscan_min_cluster_size
        self.representation_model = representation_model
        self.umap_model = None
        self.hdbscan_model = None
        self.topic_model = None

    def initialize_models(self, vectorizer_model):
        self.umap_model = UMAP(
            n_components=self.umap_n_components,
            n_neighbors=self.umap_n_neighbors,
            random_state=self.umap_random_state,
            metric=self.umap_metric,
            verbose=True,
        )
        self.hdbscan_model = hdbscan.HDBSCAN(
            min_samples=self.hdbscan_min_samples,
            gen_min_span_tree=True,
            prediction_data=True,
            min_cluster_size=self.hdbscan_min_cluster_size,
        )
        self.vectorizer_model = vectorizer_model

    def train(self, docs: t.List[str], embeddings: t.Any = None):
        self.initialize_models(vectorizer_model=self.vectorizer_model)
        self.topic_model = BERTopic(
            embedding_model=self.embedding_model,
            umap_model=self.umap_model,
            hdbscan_model=self.hdbscan_model,
            vectorizer_model=self.vectorizer_model,
            verbose=True,
            representation_model=self.representation_model,
        ).fit(docs, embeddings=embeddings)
        return self.topic_model

    def save_model(self, model_path: str, **kwargs):
        if self.topic_model is not None:
            self.topic_model.save(path=model_path, **kwargs)

    def load_model(self, model_path: str):
        self.topic_model = BERTopic.load(path=model_path)


class ModelTrainerFactory:
    _trainers = {
        "sklearn": SklearnModelTrainer,
        "gensim": GensimModelTrainer,
        "bertopic": BERTopicModelTrainer,
    }

    @staticmethod
    def create_trainer(model_type: str, **kwargs) -> ModelTrainer:
        """
        Factory method to create a model trainer.

        Args:
            model_type (str): The type of the model trainer ('sklearn', 'gensim', 'bertopic').
            **kwargs: Additional arguments required for initializing the trainer.

        Returns:
            ModelTrainer: An instance of the selected model trainer.

        Raises:
            ValueError: If the model_type is not supported.
        """
        trainer_cls = ModelTrainerFactory._trainers.get(model_type.lower())
        if not trainer_cls:
            raise ValueError(f"Unsupported model type: {model_type}")
        return trainer_cls(**kwargs)
