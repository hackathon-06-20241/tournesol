import argparse
from gensim.corpora.dictionary import Dictionary
from src.tournesol_topics.preprocessing.nlp_preprocessor import Preprocessor, SpaCyStrategy
from src.tournesol_topics.models.predict_model import BerTopicInference, GensimInference, SklearnInference


def infer_bertopic(example):
    bertop = BerTopicInference.load_model("./models/bertopic_model")
    model_inference = BerTopicInference(model=bertop)
    pred_topic = model_inference.infer(data=example)
    words = model_inference.get_topic_words(topic_id=pred_topic[0][0])
    print(words, "words for topic")


def infer_gensim(example):
    model = GensimInference.load_model("./models/gensim_model.lda")
    dictionary = Dictionary.load("./models/gensim_model.dict")
    inference = GensimInference(model=model, dictionary=dictionary)
    predicted_topics = inference.infer(data=example)
    likely_topic = sorted(predicted_topics[0], key=lambda x: x[1], reverse=True)
    most_likely_topic_id = likely_topic[0][0]
    top_words_for_topic = inference.get_topic_words(most_likely_topic_id, top_n=10)
    print(top_words_for_topic, "top words for topic")


def infer_sklearn(example):
    model = SklearnInference.load_model("./models/sklearn_topic_model.skops")
    vectorizer = SklearnInference.load_vectorizer("./models/sklearn_vectorizer.pkl")
    inference = SklearnInference(model=model, vectorizer_model=vectorizer)
    pred_topic = inference.infer([example])
    topic_id = pred_topic[0].argmax()
    words = inference.get_topic_words(topic_id=topic_id, n_words=5)
    print(words, "words for topic")


def process_text(example, language="french"):
    preprocessor = Preprocessor()
    spacy_strategy_fr = SpaCyStrategy(language)
    preprocessor.add_strategy(language=language, strategy=spacy_strategy_fr)
    return preprocessor.process_single_text(example)


def cli():
    parser = argparse.ArgumentParser(description="Infer topics using a trained model")
    parser.add_argument(
        "--model",
        type=str,
        choices=["bertopic", "gensim", "sklearn"],
        required=True,
        help="Choose the model to use for inference: 'bertopic', 'gensim', or 'sklearn'",
    )
    parser.add_argument("--text", type=str, required=True, help="Text to infer topics on.")

    args = parser.parse_args()
    return args


def main():
    args = cli()
    example = process_text(args.text)
    model = args.model
    if model == "bertopic":
        infer_bertopic(example)
    elif model == "gensim":
        infer_gensim(example)
    elif model == "sklearn":
        infer_sklearn(example)


if __name__ == "__main__":
    main()
