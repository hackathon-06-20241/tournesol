import typing as t

from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer
from gensim.models import LdaMulticore
from gensim.corpora import Dictionary
from bertopic import BERTopic

from src.tournesol_topics.models.base_topic_model import Inference


class SklearnInference(Inference):

    def __init__(self, model: LatentDirichletAllocation, vectorizer_model: CountVectorizer) -> None:
        self.model = model
        self.vectorizer_model = vectorizer_model

    def infer(self, data: t.Any):
        tf = self.vectorizer_model.transform(data)
        return self.model.transform(tf)

    def get_topic_words(self, topic_id: int, n_words=10):
        """Retrieve the top words for a given topic."""
        topic_words = self.model.components_[topic_id]
        feature_names = self.vectorizer_model.get_feature_names_out()
        top_words = [(feature_names[i], topic_words[i]) for i in topic_words.argsort()[: -n_words - 1 : -1]]
        return top_words


class GensimInference(Inference):

    def __init__(self, model: LdaMulticore, dictionary: Dictionary = None) -> None:
        self.model = model
        self.dictionary = dictionary

    def infer(self, data: t.Any):
        if self.dictionary is not None:
            corpus = self.dictionary.doc2bow(data)
            return self.model.get_document_topics(corpus, per_word_topics=True)
        corpus = [self.model.id2word.doc2bow(doc) for doc in data]
        return self.model.get_document_topics(corpus, per_word_topics=True)

    def get_topic_words(self, topic_id: int, top_n: int = 10):
        """
        Get the top words associated with a given topic.
        """
        return self.model.show_topic(topic_id, topn=top_n)


class BerTopicInference(Inference):

    def __init__(self, model: BERTopic) -> None:
        self.model = model

    def infer(self, data: t.Union[t.List[str], str]):
        return self.model.transform(data)

    def get_topic_words(self, topic_id: int):
        """Returns the words and their importance for the specified topic."""
        topic_info = self.model.get_topic(topic_id)
        return topic_info

    def get_topic_info(self):
        """Returns information about all topics."""
        return self.model.get_topic_info()
