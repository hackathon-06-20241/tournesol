from abc import ABC, abstractmethod
import typing as t
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from gensim.models.doc2vec import TaggedDocument
from gensim.models.doc2vec import Doc2Vec
from gensim.corpora import Dictionary


class Preprocessor(ABC):
    @abstractmethod
    def preprocess(self, data: t.List[str]) -> t.Any:
        pass


class BaseFeaturesBuilder:
    def build_features(self, *args, **kwargs):
        raise NotImplementedError("This method should be overridden by subclasses")


class Doc2VecFeaturesBuilder:
    def __init__(self, vector_size=100, min_count=3, window=5, epochs=10, dm=1):
        self.vector_size = vector_size
        self.min_count = min_count
        self.window = window
        self.epochs = epochs
        self.dm = dm

    def build_features(self, corpus: list, method="doc2vec"):
        """Train a document vectorizer on a list of tokens

        Args:
            tokens (pd.Series): with each line as a list of tokens to vectorize
            method (str): vectorization method.
            package (str): package to use
            doc2vec_params (dict): parameters for doc2vec model according to
                package.

        Returns:
            model (instance): trained vectorizer model with :
            vectors (pd.Dataframe) : vectors of each documents in corpus

        """

        if method == "doc2vec":
            documents = [TaggedDocument(token, [i]) for i, token in enumerate(corpus)]
            model = Doc2Vec(
                documents,
                vector_size=self.vector_size,
                min_count=self.min_count,
                window=self.window,
                epochs=self.epochs,
                dm=self.dm,
            )

            # build vocalbulary
            model.build_vocab(documents)  # but for larger corpora, consider an
            # iterable that streams the documents directly from disk/network

            # train vectorizer
            model.train(documents, total_examples=model.corpus_count, epochs=model.epochs)

            # get vectors of each documents
            vectors = pd.DataFrame([model.infer_vector(doc) for doc in corpus], index=range(len(corpus)))

        return model, vectors


class BoWFeaturesBuilder(BaseFeaturesBuilder):
    def __init__(self, method="count", **kwargs):
        self.method = method
        self.kwargs = kwargs

    def build_features(self, corpus: list) -> np.array:
        """Train and apply a bag of words vectorizer on a list of documents
            using sklearn

        Args:
            corpus (list of str): corpus of text document to vectorize
            method (str): method to use for vectorization

        Returns:
            np.array: vectorized text
        """
        if self.method == "count":
            vectorizer = CountVectorizer(**self.kwargs)
            X = vectorizer.fit_transform(corpus)

        elif self.method == "tfidf":
            vectorizer = TfidfVectorizer(**self.kwargs)
            X = vectorizer.fit_transform(corpus)
        return X


class SklearnPreprocessor(Preprocessor):
    def __init__(self, stop_words, vocab, max_features=20000, max_df=0.95, min_df=2, ngram_range=(1, 3)):
        self.vectorizer = CountVectorizer(
            stop_words=stop_words,
            max_features=max_features,
            max_df=max_df,
            min_df=min_df,
            vocabulary=vocab,
            ngram_range=ngram_range,
        )

    def preprocess(self, data: t.List[str]):
        return self.vectorizer.fit_transform([" ".join(d) for d in data])


class GensimPreprocessor(Preprocessor):
    def __init__(self):
        self.dictionary = Dictionary()

    def preprocess(self, data: t.List[str]):
        self.dictionary.add_documents(documents=data)
        self.dictionary.filter_extremes(no_below=15, no_above=0.25)
        return [self.dictionary.doc2bow(text) for text in data]
