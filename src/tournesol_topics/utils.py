# -*- coding: utf-8 -*-
""" Utility functions """
import os
import json
import re
import time
import shutil
import string
import unidecode
import typing as t
from pathlib import Path
from contextlib import contextmanager
from loguru import logger
import pandas as pd


# filepaths
def check_file_exists(file_paths: t.List[str]):
    """check for file existance in a list of paths

    Args:
        file_paths (list): list of paths to test

    Returns:
        str: identified file path
        None: if no file was found
    """
    for file_path in file_paths:
        if os.path.exists(file_path):
            return file_path
        else:
            return None


# lists operations
def common_elements(l1: list, l2: list) -> list:
    """finds common elements in two lists

    Args:
        l1 (list): 1st list
        l2 (list): 2nd list

    Returns:
        list: common elements
    """
    return list(set(l1) & set(l2))


def unique_elements(l1: list, l2: list, output="nested"):
    """finds common elements in two lists
    Warning: do not preserve lists elements order

    Args:
        l1 (list): 1st list
        l2 (list): 2nd list
        output (str): how to output elements from each list:
            "nested" : each in disctint sub lists
            "together": in the same list

    Returns:
        list: common elements
    """
    if output == "nested":
        return [list(set(l1) - set(l2)), list(set(l2) - set(l1))]
    elif output == "together":
        return list(set(l1) - set(l2)) + list(set(l2) - set(l1))


def timer(func: t.Callable):
    """
    Timer decorator to measure function execution time
    Args :
        func (function): function to time
    Returns:
        wrapper (function): wrapped fonction
    """

    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        elapsed = end - start
        logger.info(f"Execution time: {time.strftime('%H:%M:%S', time.gmtime(elapsed))}")
        return result, elapsed

    return wrapper


def load_json(file_path: str):
    """Load json file

    Args:
        file_path (str): path to json file

    Returns:
        dict: json file content
    """
    with open(file_path, "r", encoding="utf-8") as f:
        return json.load(f)


def load_jsonl(file_path: str):
    """
    Loads a JSONL (JSON Lines) file and returns a list of dictionaries.

    Parameters:
    file_path (str): Path to the JSONL file.

    Returns:
    list[dict]: List of dictionaries, where each dictionary represents a JSON object.
    """

    # TODO: Use Generator instead of list
    with open(file_path, "r", encoding="utf-8") as f:
        return [json.loads(line.strip()) for line in f]


def load_text(file_path: str | Path) -> str:
    with open(file_path, "r", encoding="utf-8") as f:
        return f.read()


@contextmanager
def write_and_rename(data_path: str, mode: str = "w", suffix: str = ".tmp", encoding="utf-8"):
    temporary_file_path = f"{data_path}{suffix}"
    with open(temporary_file_path, mode, encoding=encoding) as fp:
        yield fp
    if os.path.isfile(data_path):
        os.remove(data_path)
        time.sleep(0.1)
    shutil.move(temporary_file_path, data_path)


def save_jsonl(data: list[dict], file_path: str):
    with write_and_rename(file_path) as fp:
        for d in data:
            fp.write(json.dumps(d) + "\n")


def chunk_docs(docs: list[str], chunk_size: int) -> list[list[str]]:
    return [docs[i : i + chunk_size] for i in range(0, len(docs), chunk_size)]


def process_string(text: str, **options):
    # Default options
    defaults = {
        "remove_punctuation": True,
        "lower_case": True,
        "remove_accents": True,
        "remove_numbers": True,
        "remove_links": False,
        "special_characters_list": [],
    }
    opts = {**defaults, **options}
    if not any(opts.values()) or not text:
        return text
    # Remove punctuation
    if opts["remove_punctuation"]:
        text = text.translate(str.maketrans("", "", string.punctuation))

    # Convert to lower case
    if opts["lower_case"]:
        text = text.lower()

    # Remove special characters
    for char in opts["special_characters_list"]:
        text = text.replace(char, "")

    # Remove accents
    if opts["remove_accents"]:
        text = unidecode.unidecode(text)

    # Remove numbers
    if opts["remove_numbers"]:
        text = re.sub(r"\d+", "", text)

    # Remove links
    if opts["remove_links"]:
        text = re.sub(r"http\S+", "", text)

    # Replace specific characters
    replace_table = str.maketrans({"&": "and", "@": "at", "€": "euro"})
    text = text.translate(replace_table)

    return text


def filter_df(df: pd.DataFrame) -> list:
    df.rename(columns={"transcript": "transcripts"}, inplace=True)
    df = df.dropna(subset=["transcripts"])
    return df.to_dict("records")
