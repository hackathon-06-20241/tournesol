import typing as t
from enum import Enum
from dataclasses import dataclass


@dataclass
class StrategyConfig:
    """Configuration for text processing strategies"""

    language: str
    additional_stopwords: t.Optional[list] = None
    batch_size: int = 30
    n_process: int = 1
    disable_components: t.Optional[list] = None

    def to_dict(self) -> dict:
        return {k: v for k, v in self.__dict__.items() if v is not None}


class StrategyType(str, Enum):
    """Enum for supported strategy types"""

    SPACY = "spacy"
    NLTK = "nltk"

    @classmethod
    def list(cls) -> list[str]:
        return [member.value for member in cls]


@dataclass
class LanguageStrategyConfig:
    """Configuration for language-specific strategy"""

    strategy_type: StrategyType
    additional_stopwords: t.Optional[list] = None
    batch_size: int = 30
    n_process: int = 1


class TopicInfo(t.TypedDict):
    topics: t.List[t.List[str]]
    docs: t.List[t.List[str]]


class EvalScore(t.TypedDict):
    coherence_npmi: float
    coherence_cv: float
    diversity: float
