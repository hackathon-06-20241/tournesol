import typing as t


SPACY_DEFAULT_POS_TAGS: t.Set[str] = {
    "ADV",
    "PRON",
    "CCONJ",
    "PUNCT",
    "DET",
    "ADP",
    "SPACE",
    "ADJ",
    "VERB",
}

NLTK_DEFAULT_POS_TAGS: t.Set[str] = {
    "RB",
    "PRP",
    "CC",
    "DT",
    "IN",
    "JJ",
    "VB",
}

LANGUAGES: t.Set[str] = {"english", "french", "german", "espanish", "italian"}


SPACY_LANGUAGE_MODELS = {
    "en": "en_core_web_md",
    "fr": "fr_core_news_md",
    "de": "de_core_news_md",
    "es": "es_core_news_md",
    "it": "it_core_news_sm",
}


LANGUAGES_MAP = {
    "english": "en",
    "french": "fr",
    "german": "de",
    "spanish": "es",
    "italian": "it",
}
