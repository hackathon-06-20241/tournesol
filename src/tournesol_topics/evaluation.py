from functools import partial
import typing as t
from gensim.models import CoherenceModel
from gensim.corpora import Dictionary
from tournesol_topics import types as tp
from tournesol_topics.base_metric import AbstractMetric


class Coherence(AbstractMetric):
    def __init__(
        self, texts: t.List[t.List[str]], topk: int = 10, processes: int = 1, measure: str = "c_npmi"
    ):
        """
        Initialize metric

        Parameters
        ----------
        texts(List[List[str]]) : list of documents (list of lists of strings)
        topk (int): how many most likely words to consider in
        the evaluation
        measure (str) : (default 'c_npmi') measure to use.
        processes (int): number of processes
        other measures: 'u_mass', 'c_v', 'c_uci', 'c_npmi'
        """
        super().__init__()
        if texts is None:
            raise ValueError("Texts must be provided")
        else:
            self._texts = texts
        self._dictionary = Dictionary(self._texts)
        self.topk = topk
        self.processes = processes
        self.measure = measure

    def info(self):
        return {"name": "Coherence"}

    def score(self, model_output: t.Dict[str, t.List[t.List[str]]]):
        """
        Retrieve the score of the metric

        Parameters
        ----------
        model_output : dictionary, output of the model
                       key 'topics' required.

        Returns
        -------
        score : coherence score
        """
        topics = model_output["topics"]
        if topics is None:
            return -1
        if self.topk > len(topics[0]):
            raise Exception("Words in topics are less than topk")
        else:
            npmi = CoherenceModel(
                topics=topics,
                texts=self._texts,
                dictionary=self._dictionary,
                coherence=self.measure,
                processes=self.processes,
                topn=self.topk,
            )
            return npmi.get_coherence()


class TopicDiversity(AbstractMetric):
    def __init__(self, topk=10):
        """
        Initialize metric

        Parameters
        ----------
        topk: top k words on which the topic diversity will be computed
        """
        AbstractMetric.__init__(self)
        self.topk = topk

    def info(self):
        return {"name": "Topic diversity"}

    def score(self, model_output):
        """
        Retrieves the score of the metric

        Parameters
        ----------
        model_output : dictionary, output of the model
                       key 'topics' required.

        Returns
        -------
        td : score
        """
        topics = model_output["topics"]
        if topics is None:
            return 0
        if self.topk > len(topics[0]):
            raise Exception("Words in topics are less than " + str(self.topk))
        else:
            unique_words = set()
            for topic in topics:
                unique_words = unique_words.union(set(topic[: self.topk]))
            td = len(unique_words) / (self.topk * len(topics))
            return td


def evaluator(topics: tp.TopicInfo, topk: int = 10) -> tp.EvalScore:
    """
    Evaluate the given topics using the following metrics:

    - Coherence (npmi)
    - Coherence (c_v)
    - Topic diversity

    Parameters
    ----------
    topics : tp.TopicInfo
        The topics to be evaluated
    topk : int, optional
        The number of top words to consider for coherence and diversity metrics
        by default 10

    Returns
    -------
    tp.EvalScore
        A named tuple containing the scores for the three metrics
    """
    partial_coherence = partial(Coherence, texts=topics["docs"], topk=topk)
    coherence_npmi = partial_coherence().score(topics)
    coherence_cv = partial_coherence(measure="c_v").score(topics)
    diversity = TopicDiversity().score(model_output=topics)
    return tp.EvalScore(coherence_cv=coherence_cv, coherence_npmi=coherence_npmi, diversity=diversity)
