import os
import json
import argparse
from tqdm import tqdm
import numpy as np
from nltk.corpus import stopwords
from gensim.corpora.dictionary import Dictionary
from src.tournesol_topics.models.train_model import (
    GensimModelTrainer,
    TopicModelPreprocessor,
    BERTopicModelTrainer,
)
from src.tournesol_topics.models.train_model import SklearnModelTrainer


def load_data(file_path: str):
    """Loads the cleaned data from a JSON file."""
    with open(file_path, "r") as f:
        data = json.load(f)
    return [d["transcripts"] for d in tqdm(data)]


def load_or_train_model(trainer, docs=None, embeddings=None, model_path=None, force_train=False):
    """
    Load a model if it exists, otherwise train and save it. Optionally force retraining.

    Args:
        trainer: The trainer object that can either load or train the model.
        docs: The documents to train the model on (optional).
        embeddings: The embeddings to use for training (optional).
        model_path: The file path where the model is saved.
        force_train: If True, the model will be retrained even if it exists (default: False).

    Returns:
        The trainer with the loaded or trained model.
    """
    if force_train or not os.path.exists(model_path):
        if docs is None:
            raise ValueError("Documents are required for training if the model does not exist.")
        if embeddings is not None:
            trainer.train(docs=docs, embeddings=embeddings)
        else:
            trainer.train(docs=docs)
        trainer.save_model(model_path)
    else:
        trainer.load_model(model_path)

    return trainer


def run_sklearn(force_train=False):
    docs = load_data("./cleaned_data_french.json")
    preprocessor = TopicModelPreprocessor(stop_words=stopwords.words("french"))
    vectorizer_model = preprocessor.preprocess(docs)
    docs = preprocessor.process_docs(docs)
    trainer = SklearnModelTrainer(vectorizer_model=vectorizer_model)
    model_path = "./models/sklearn_topic_model.skops"

    return (
        load_or_train_model(trainer=trainer, docs=docs, model_path=model_path, force_train=force_train),
        vectorizer_model,
    )


def run_bertopic(force_train=False):
    with open("./cleaned_data_french.json", "r") as f:
        data = json.load(f)
    docs = [d["transcripts"] for d in tqdm(data)]
    preprocessor = TopicModelPreprocessor(stop_words=stopwords.words("french"))
    embeddings = np.load("./notebooks/topic_modelling/mpnet-embeddings.npy")
    vectorizer_model = preprocessor.preprocess(docs)
    docs = preprocessor.process_docs(docs)
    trainer = BERTopicModelTrainer()
    model_path = "./models/bertopic_model"

    return load_or_train_model(
        trainer=trainer,
        docs=docs,
        embeddings=embeddings,
        model_path=model_path,
        force_train=force_train,
    )


def run_gensim(force_train=False):
    docs = load_data("./cleaned_data_french.json")
    dictionary = Dictionary([d for d in docs])
    trainer = GensimModelTrainer()
    model_path = "./models/gensim_model.lda"

    return (
        load_or_train_model(trainer=trainer, docs=docs, model_path=model_path, force_train=force_train),
        dictionary,
    )


def cli():
    parser = argparse.ArgumentParser(description="Run a specific topic model")
    parser.add_argument(
        "--model",
        type=str,
        choices=["bertopic", "gensim", "sklearn"],
        required=True,
        help="Choose the model to run: 'bertopic', 'gensim', or 'sklearn'",
    )
    parser.add_argument(
        "--force-train",
        action="store_true",
        help="Force the model to be retrained even if it exists.",
    )

    args = parser.parse_args()
    return args


def main():
    args = cli()
    model_type = args.model
    force_train = args.force_train
    if model_type == "bertopic":
        bertop = run_bertopic(force_train=force_train)

    elif model_type == "gensim":
        model, dictionary = run_gensim(force_train=force_train)

    elif model_type == "sklearn":
        bertop, vectorizer = run_sklearn(force_train=force_train)


if __name__ == "__main__":
    main()
