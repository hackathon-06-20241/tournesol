import typer
from pathlib import Path
import pandas as pd
from tournesol_topics import utils
from tournesol_topics import types as tp
from tournesol_topics.preprocessing.strategies import StrategyFactory
from tournesol_topics.preprocessing.nlp_preprocessor import Preprocessor


def preprocess_data(filename: str, var_name: str, output_fp: str, save: bool) -> list[dict]:
    """Runs preprocessing on the specified data and saves the output.

    Args:
        filename (str): The filename to preprocess the data.
        var_name (str): The name of the variable to preprocess.
        output_fp (str): The output filename to save the preprocessed data.
        save (bool): Whether to save the preprocessed data.

    Returns:
        list[dict]: The processed corpus.
    """
    fp = Path(filename)
    suffix = fp.suffix
    if suffix == ".jsonl":
        corpus: list[dict] = utils.load_jsonl(filename)
    elif suffix == ".parquet":
        corpus = pd.read_parquet(filename).to_dict("records")

    strategy_factory = StrategyFactory()
    preprocessor = Preprocessor(strategy_factory=strategy_factory)

    # Load additional stopwords
    additional_stopwords = utils.load_text("data/additional_stopwords.txt").splitlines()
    additional_stopwords = [s.strip() for s in additional_stopwords]

    strategy_mapping = {
        "english": (tp.StrategyType.SPACY, None),
        "french": (tp.StrategyType.SPACY, additional_stopwords),
    }
    processed_corpus = preprocessor.run(
        data=corpus,
        var_name=var_name,
        save=save,
        strategy_mapping=strategy_mapping,
        filename=output_fp if save else None,
    )

    return processed_corpus


def run_single_preprocess(text: str):
    """Preprocesses a single text input using the preprocessor.

    Args:
        text (str): The text to preprocess.

    Returns:
        dict: The processed text.
    """
    strategy_factory = StrategyFactory()
    preprocessor = Preprocessor(strategy_factory=strategy_factory)

    return preprocessor.process_single_text(text, "french")


def main(
    input_fp: str = typer.Option(..., help="The filename of the data to preprocess."),
    var_name: str = typer.Option("transcripts", help="The name of the variable to preprocess."),
    output_fp: str = typer.Option(
        "preprocessed_data.jsonl", help="The filename to save the preprocessed data."
    ),
    save: bool = typer.Option(True, "--save/--no-save", help="Whether to save the preprocessed data."),
):
    """CLI entry point for preprocessing Tournesol transcripts."""
    fp = Path(input_fp)
    if not fp.exists():
        raise ValueError(f"File {input_fp} does not exist.")
    preprocess_data(filename=input_fp, var_name=var_name, output_fp=output_fp, save=save)


if __name__ == "__main__":
    typer.run(main)
