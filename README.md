# Tournesol

## Description du projet
Sur la base des transcripts des vidéos notées sur la plateforme Tournesol, mise en place d'un projet de traitement du langage.
L'objectif de ce projet est l'extraction des thèmes clés des vidéos sur la base des transcript (speech to text) des vidéos. Cela permet d'identifier les mots clés et les intentions des vidéos, de les catégoriser, d'ajouter un module de sélection par thème, analyser les scores par thèmes...
Ce projet est la suite du projet initié pendant le [Hackathon 2023](https://gitlab.com/hackathon-2023-03-tournesol/video_theme_sentiment)


## Requirements
1. Pour commencer créer un environnement virtuel : 
    - par exemple avec conda : `conda create -n nom-environnement`
    - ou avec pip : `python3 -m venv nom-environnement`

2. Activer l'environnement virtuel  et installez les dépendances avec la commande : `pip install -e .`  


## Objectifs & Livrables
Développez un algorithmes capable de prédire des thèmatiques présentes dans des transcriptions de vidéos

## Données disponibles

0. Vous pouvez utilisez des données pré-existantes (transcripts de videos)
    En chargeant les données via cet url : 
    - [données brutes de 2023](https://filedn.eu/lefeldrXcsSFgCcgc48eaLY/datasets/Tournesol/all-videos-with-transcripts-and-tokens-fr.json)
    - [sur huggingface pour 2023](https://huggingface.co/datasets/madoss/tournesol_transcripts_2023)
    - [sur huggingface pour 2024](https://huggingface.co/datasets/madoss/tournesol_transcripts_2024)
    - [Données nettoyées pour 2023 et 2024](https://huggingface.co/datasets/madoss/tournesol_fr_preprocessed_transcripts)

Si vous optez pour les données sur Huggingface, vous pouver télécharger le format `.parquet` en vous rendant sur les liens au dessus.

Pour lancer un prétraitement, utilisez la commande suivante:
``python scripts\process_corpus.py --input-fp transcripts_2024.parquet --input-fp path_to_input.parquet --output-fp path_to_output.jsonl``

Où `path_to_input.parquet` est le chemin vers le fichier `.parquet` contenant les données brutes et `path_to_output.jsonl` est le chemin vers le fichier `.jsonl` contenant les données nettoyées. Le format de `--input-fp` peut etre `.parquet` ou `.jsonl` et le format de `-output-fp` doit toujours etre `.jsonl`

1. Collectez des transcripts avec l'API de Tournesol  
    1.1 Séparez vos données en deux d'un datasets:  un de **train** et un de **test** : il serait intéressant de labeliser les transcripts de vidéos du data set de test afin d'évaluer la performance de vos algorithmes une fois entraînés

2. Faire les pré-traitements des transcripts de votre corpus
Vous pouvez utiliser le script ``src/tournesol_topics/processing/nlp_preprocessor.py`` pour nettoyer les données et leur appliquer une vectorisation (simple bag of word ou word embedding)


3. Entrainement de modèles de machine learning pour le topic modeling
    4.1 Un algorithme basé sur la [Latent Dirichlet Allocation](https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.LatentDirichletAllocation.html) de ``Scikit-Learn`` est implementé dans ``src/tournesol_topics/models/train_model.py`` dans la classe ``SklearnModelTrainer``
    4.2 Un algorithme basé sur ``Gensim`` est implementé dans ``src/tournesol_topics/models/train_model.py`` dans la classe ``GensimModelTrainer``
    4.3 Un algorithme basé sur ``BERTopic`` est implementé dans ``src/tournesol_topics/models/train_model.py`` dans la classe ``BERTopicModelTrainer``

5. Testez votre algorithmes entrainé en calculant des performances sur le jeu de test

## Collaboration
Toutes les nouvelles fonctionnalités sont listées dans les "Issues" de Gitlab, chacun peut contribuer en s'attribuant une issue et en créant une branche associée (avec le même nom que l'issue)

## Organisation du repo
`data`: dossier contenant vos données   
`docs`: dossier contenant la documentation pour votre projet  
`notebooks`: dossier contenant vos notebooks pour l'exploration des données  
`src`: dossier contenant votre code source : mettez y vos scripts  